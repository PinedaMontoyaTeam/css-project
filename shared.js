let backdrop = document.querySelector('.backdrop');
let modal = document.querySelector('.modal');
let selectPlanButtons = document.querySelectorAll('.plan button');
let modalNegativeBtn = document.querySelector('.modal__action--negative');
let toggleButton = document.querySelector('.toggle-button');
let mobileNav = document.querySelector('.mobile-nav');
let ctaButton = document.querySelector('.main-nav__item--cta');

for(let i = 0; i < selectPlanButtons.length; i++) {
    selectPlanButtons[i].addEventListener('click', function(){
        modal.classList.add('open'); 
        backdrop.style.display = 'block';
        setTimeout(function() {
            backdrop.classList.add('open');
        }, 10)       
    });
}
backdrop.addEventListener('click', function() {
    mobileNav.classList.remove('open');
    closeModal();
});

if(modalNegativeBtn) {
    modalNegativeBtn.addEventListener('click', closeModal);
}

function closeModal() {
    if(modal) {
        modal.classList.remove('open');
    }
    backdrop.classList.remove('open');
    setTimeout(function() {
        backdrop.style.display = 'none';
    }, 200)
}

toggleButton.addEventListener('click', function() {
    mobileNav.classList.add('open');
    backdrop.style.display = 'block';
    setTimeout(function() {
        backdrop.classList.add('open');
    }, 10)
})

ctaButton.addEventListener('animationstart', function(event) {
    console.log('Animation started', event)
})

ctaButton.addEventListener('animationend', function(event) {
    console.log('Animation ended', event)
})

ctaButton.addEventListener('animationiteration', function(event) {
    console.log('Animation iteration', event)
})